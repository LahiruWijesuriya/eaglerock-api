# EagleRock API

## Overview
This project contains the EagleRock API, a C#/.NET Core application designed to manage Todo items. The solution includes Docker and Kubernetes configurations for containerization and deployment.
The project provides two methods for deployment:

1. PowerShell Script
2. GitLab CI/CD Pipeline

## Prerequisites
- .NET Core SDK 3.0
- Docker
- Kubernetes (Enabled in Docker Desktop)
- PowerShell (for Windows users)
- GitLab CI/CD (for pipeline-based deployment)


## Project Structure
- `TodoApi/`: Contains the C# API project.
- `XUnitTests/`: Contains unit tests for the API project.
- `k8s-deployment.yaml`: Kubernetes Deployment configuration. -> Defines the deployment of the EagleRock API with three replicas, health checks, and resource requests/limits.
- `k8s-service.yaml`: Kubernetes Service configuration. -> Defines a LoadBalancer service to expose the EagleRock API.
- `k8s-hpa.yaml`: Kubernetes Horizontal Pod Autoscaler configuration. -> Defines the HPA configuration to scale the deployment based on CPU utilization.
- `Dockerfile`: Dockerfile for the API project.
- `Dockerfile.kubeval`: Dockerfile for building the kubeval validation image (used within Gitlab pipeline as we're not deploying real clusters).
- `build-deploy.ps1`: PowerShell script for building, testing, and deploying the application locally.
- `.gitlab-ci.yml`: GitLab CI/CD pipeline configuration file.

### Setup and Deployment via GitLab CI/CD Pipeline ### 
The .gitlab-ci.yml file defines a CI/CD pipeline that builds, tests, and deploys the application using GitLab CI/CD.

Steps to Set Up
1. Create a new repository on GitLab. (or refer to exisiting Project https://gitlab.com/LahiruWijesuriya/eaglerock-api)
2. Upload the project files to the repository.
3. Ensure that the following CI/CD variables are set in the GitLab project settings:
    - CI_REGISTRY_USER: GitLab username.
    - CI_REGISTRY_PASSWORD: A GitLab personal access token with appropriate permissions.

Pipeline Stages
1. build: Builds the .NET application.
2. test: Runs unit tests.
3. docker: Builds and pushes the Docker image to the GitLab Container Registry.
4. build-kubeval-image: Builds and pushes a custom Docker image for kubeval validation.
5. deploy: Uses the custom kubeval image to validate Kubernetes YAML files.

Note: Deploy stage uses kubeval to evaluate the kubernetes file as we're not deploying any clusters. Despite this, the stage is failing as kubeval thinks its running with the '-c' flag when it isnt.. I won't go down the rabbit hole much further on this.. 'Error: unknown shorthand flag: 'c' in -c'

Running the Pipeline
The pipeline runs automatically on every commit to the main branch. The deploy stage will fail as there are no Kubernetes clusters deployed
 - Pipeline from existing GitLab project: https://gitlab.com/LahiruWijesuriya/eaglerock-api/-/pipelines

### Setup and Deployment via PowerShell Script ###

The PowerShell Script:
    Restores and builds the .NET project.
    Runs unit tests.
    Builds a Docker image for the API.
    Pushes the Docker image to Docker Hub after being prompted to input your Docker username and password.
    Deploys the application to a local Docker Desktop Kubernetes cluster.
    Validates the deployment and tests the API endpoint.


### Step 1: Setting up Kubernetes via Docker Desktop
Docker Desktop Kubernetes provides a simple way to run a local Kubernetes cluster.
By using Docker Desktop Kubernetes, we've avoided the need for complex setup and configuration of separate Kubernetes clusters, making the development workflow more efficient.

I initially developed this with a minikube tunnel, but realised its best to strip it down to the bare minimum.

### Installing Docker Desktop

1. **Download Docker Desktop:**
   - Download Docker Desktop from [Docker's official website](https://www.docker.com/products/docker-desktop).

2. **Install Docker Desktop:**
   - Follow the installation instructions for your operating system (Windows or macOS).

### Enabling Kubernetes in Docker Desktop

1. **Open Docker Desktop:**
   - Launch Docker Desktop from your system.

2. **Enable Kubernetes:**
   - Click on the gear icon to go to **Settings**.
   - In the **Settings** window, navigate to the **Kubernetes** tab.
   - Check the box labeled **Enable Kubernetes**.
   - Click **Apply & Restart** to start the Kubernetes cluster.

### Verifying Kubernetes Setup

1. **Open a Terminal:**
   - Open a terminal window (Command Prompt, PowerShell, or any terminal emulator).

2. **Check Kubernetes Version:**
```sh
kubectl version --client
kubectl version
```

## Step 2: Run the PowerShell Script
In the terminal, 
1. Navigate to the root of the project directory.
2. Run the following command to execute the script:

```sh
.\build-deploy.ps1
```

## Manually Verifying the Deployment via TodoAPI.sln 
Though the script incorporates verification steps, we can do it manually.
After running the script, verify the deployment by checking the running pods and the service status. The script will log the number of running pods and test the API endpoint.

- Manually test the API, use:
```sh
kubectl get svc eaglesrock-api-service
```
sh

- Build and run the TodoAPI.sln via Visual Studio Code
   1.  Open Solution in Visual Studio:
      Launch Visual Studio.
      Go to File > Open > Project/Solution.
      Navigate to and select TodoAPI.sln.

   2. Build the Solution:
      Use Build > Build Solution to compile all projects in the solution.

   3. Run the Solution:
      Set the desired project as the startup project (e.g., TodoApi).
      Use Debug > Start Debugging or press F5 to run the application.

- Use the provided IP to test the API endpoint:
```sh
curl http://127.0.0.1:5001/api/TodoItems
```
This should return an empty 'Content' value
Note: The Kubernetes Service configuration port is elected as 5001 is the port used by the container inside the pod, while 30001 is the NodePort that Kubernetes exposes for accessing the service externally.

- Check and test application on browser and add some ToDo Items via the webapp
http://localhost:5001/ 
Add some to-do items

- Curl the provided IP to test the API endpoint with the new items:
```sh
curl http://127.0.0.1:5001/api/TodoItems
```
This should return a populated 'Content' value