# Define variables
$projectName = "TodoApi"
$deploymentName = "eaglesrock-api-deployment"
$serviceName = "eaglesrock-api-service"
$nodePort = 30001  # NodePort exposed for the service
$deploymentFilePath = "k8s-deployment.yaml"

# Function to log messages with a timestamp
function Log-Message {
    param (
        [string]$message
    )
    $timestamp = Get-Date -Format "yyyy-MM-dd HH:mm:ss"
    Write-Host "[$timestamp] $message"
}

# Function to replace placeholders in files
function Replace-Placeholder {
    param (
        [string]$filePath,
        [string]$placeholder,
        [string]$replacement
    )
    (Get-Content $filePath) -replace $placeholder, $replacement | Set-Content $filePath
}

# Function to revert placeholders in files
function Revert-Placeholder {
    param (
        [string]$filePath,
        [string]$placeholder,
        [string]$original
    )
    (Get-Content $filePath) -replace $placeholder, $original | Set-Content $filePath
}

# Prompt for Docker Hub credentials
$dockerUsername = Read-Host "Enter Docker Hub Username"
$dockerPassword = Read-Host "Enter Docker Hub Password" -AsSecureString

# Define dockerImageName using the provided username
$dockerImageName = "$dockerUsername/eaglesrock-api:latest"

# Step 1: Verify Dockerfile
Log-Message "Starting Step 1: Verify Dockerfile..."

# Verify the existence of Dockerfile
if (Test-Path "$projectName/Dockerfile") {
    Log-Message "Dockerfile exists."
} else {
    Log-Message "Dockerfile does not exist. Please create the Dockerfile and try again."
    exit 1
}

Log-Message "Dockerfile verification completed. Step 1 completed successfully."

# Step 2: Build and Push Docker Image
Log-Message "Starting Step 2: Build and Push Docker Image..."

dotnet restore $projectName
if ($?) {
    Log-Message "dotnet restore completed successfully."
} else {
    Log-Message "dotnet restore failed."
    exit 1
}

dotnet build $projectName --configuration Release
if ($?) {
    Log-Message "dotnet build completed successfully."
} else {
    Log-Message "dotnet build failed."
    exit 1
}

Log-Message "Running unit tests..."
dotnet test XUnitTests/XUnitTests.csproj
if ($?) {
    Log-Message "Unit tests passed successfully."
} else {
    Log-Message "Unit tests failed."
    exit 1
}

Log-Message "Building Docker image..."
docker build -t $dockerImageName -f "$projectName/Dockerfile" .
if ($?) {
    Log-Message "Docker image built successfully."
} else {
    Log-Message "Docker image build failed."
    exit 1
}

Log-Message "Pushing Docker image to Docker Hub..."
$dockerPasswordPlainText = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($dockerPassword))
docker login -u $dockerUsername -p $dockerPasswordPlainText
if ($?) {
    Log-Message "Docker login succeeded."
} else {
    Log-Message "Docker login failed."
    exit 1
}

docker push $dockerImageName
if ($?) {
    Log-Message "Docker image pushed successfully."
} else {
    Log-Message "Docker image push failed."
    exit 1
}

Log-Message "Step 2 completed: Docker image built and pushed successfully."

# Step 3: Deploy to Kubernetes
Log-Message "Starting Step 3: Deploy to Kubernetes..."

# Replace the placeholder in k8s-deployment.yaml with the actual image name
Replace-Placeholder $deploymentFilePath 'PLACEHOLDER_IMAGE_NAME' $dockerImageName

kubectl apply -f k8s-deployment.yaml
if ($?) {
    Log-Message "Kubernetes deployment applied successfully."
} else {
    Log-Message "Kubernetes deployment apply failed."
    exit 1
}

kubectl apply -f k8s-service.yaml
if ($?) {
    Log-Message "Kubernetes service applied successfully."
} else {
    Log-Message "Kubernetes service apply failed."
    exit 1
}

kubectl apply -f k8s-hpa.yaml
if ($?) {
    Log-Message "Kubernetes HPA applied successfully."
} else {
    Log-Message "Kubernetes HPA apply failed."
    exit 1
}

# Verify deployment status
Log-Message "Verifying deployment status..."
$maxRetries = 30
$retryCount = 0
$sleepDuration = 10

# Function to check pod status
function Check-PodStatus {
    $pods = kubectl get pods --selector=app=eaglesrock-api -o jsonpath='{.items[*].status.phase}'
    $runningPods = $pods -split ' ' | Where-Object { $_ -eq 'Running' }
    if ($runningPods.Count -eq (kubectl get pods --selector=app=eaglesrock-api --no-headers | Measure-Object).Count) {
        return $true
    }
    return $false
}

# Wait loop for pod status
while ($retryCount -lt $maxRetries) {
    if (Check-PodStatus) {
        Log-Message "All pods are running."
        break
    } else {
        Log-Message "Waiting for pods to be in the running state..."
        Start-Sleep -Seconds $sleepDuration
        $retryCount++
    }
}

if ($retryCount -eq $maxRetries) {
    Log-Message "Some pods are not running. Please check the deployment."
    kubectl get pods
    exit 1
}

Log-Message "All pods are running. Step 3 completed: Deployment successful."

# Verify Service Availability
Log-Message "Verifying service availability..."
$service = kubectl get svc $serviceName -o jsonpath='{.spec.ports[0].nodePort}'
if ($null -ne $service) {
    Log-Message "Service is available at NodePort: $service"
} else {
    Log-Message "Failed to get NodePort for the service. Please check the service configuration."
    kubectl get svc $serviceName
    exit 1
}

# Log number of running pods
Log-Message "Logging number of running pods..."
$pods = kubectl get pods --selector=app=eaglesrock-api -o json
$podCount = ($pods | ConvertFrom-Json).items.Count
Log-Message "Number of running pods: $podCount"

# Final verification: Testing the API endpoint
Log-Message "Starting final verification: Testing the API endpoint..."
$response = Invoke-WebRequest -Uri http://localhost:30001/api/TodoItems -UseBasicParsing
if ($response.Content -eq "[]") {
    Log-Message "API endpoint test successful: Received expected response."
} else {
    Log-Message "API endpoint test failed: Unexpected response."
    Log-Message "Response content: $($response.Content)"
    exit 1
}

Log-Message "Deployment verification completed successfully. All steps are complete."

# Revert the placeholder in k8s-deployment.yaml
Revert-Placeholder $deploymentFilePath $dockerImageName 'PLACEHOLDER_IMAGE_NAME'
